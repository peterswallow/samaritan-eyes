﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Samaritan.Dummy.Properties;

namespace Samaritan.Dummy
{
    public partial class SamApp : Form
    {
        public SamApp()
        {
            InitializeComponent();
            this.cbStatus.SelectedIndex = 0;
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetImage();
        }
        
        private void SetImage()
        {
            switch (this.cbStatus.SelectedIndex)
            {
                case 0:
                    this.pbImage.Image = Resources.black;
                    break;
                case 1:
                    this.pbImage.Image = Resources.yellow;
                    break;
                case 2:
                    this.pbImage.Image = Resources.green;
                    break;
                case 3:
                    this.pbImage.Image = Resources.blue;
                    break;
                case 4:
                    this.pbImage.Image = Resources.red;
                    break;
                default:
                    throw new Exception("Status not recognised");
            }
            
        }

        private void SamApp_Load(object sender, EventArgs e)
        {

        }
    }
}
