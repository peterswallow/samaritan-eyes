﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Samaritan.Eyes
{
    //https://stackoverflow.com/questions/15413172/capture-a-keyboard-keypress-in-the-background
    public static class HotkeyService
    {
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);

        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        public const int TOGGLE_STATUS_HOTKEY_ID = 1;

        public static void RegisterToggleStatusHotkey(IntPtr handle)
        {
            // Modifier keys codes: Alt = 1, Ctrl = 2, Shift = 4, Win = 8
            // Compute the addition of each combination of the keys you want to be pressed
            // ALT+CTRL = 1 + 2 = 3 , CTRL+SHIFT = 2 + 4 = 6...
            RegisterHotKey(handle, TOGGLE_STATUS_HOTKEY_ID, 6, (int)Keys.F12);
        }

        public static void DetectHotkeys(ref Message m)
        {
            if (m.Msg == 0x0312 && m.WParam.ToInt32() == TOGGLE_STATUS_HOTKEY_ID)
            {
                SoundService.SayText("F 12 pressed");
            }            
        }

    }
}
