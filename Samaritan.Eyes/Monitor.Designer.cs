﻿namespace Samaritan.Eyes
{
    partial class Monitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_ReadStatusAloud = new System.Windows.Forms.Button();
            this.lbCurrentStatus = new System.Windows.Forms.Label();
            this.btSetAvailable = new System.Windows.Forms.Button();
            this.btSetUnavailable = new System.Windows.Forms.Button();
            this.lbCurrentStatusTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bt_ReadStatusAloud
            // 
            this.bt_ReadStatusAloud.Location = new System.Drawing.Point(302, 16);
            this.bt_ReadStatusAloud.Name = "bt_ReadStatusAloud";
            this.bt_ReadStatusAloud.Size = new System.Drawing.Size(86, 23);
            this.bt_ReadStatusAloud.TabIndex = 1;
            this.bt_ReadStatusAloud.Text = "Read Aloud";
            this.bt_ReadStatusAloud.UseVisualStyleBackColor = true;
            this.bt_ReadStatusAloud.Click += new System.EventHandler(this.bt_ReadStatusAloud_Click);
            // 
            // lbCurrentStatus
            // 
            this.lbCurrentStatus.AutoSize = true;
            this.lbCurrentStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentStatus.Location = new System.Drawing.Point(12, 55);
            this.lbCurrentStatus.Name = "lbCurrentStatus";
            this.lbCurrentStatus.Size = new System.Drawing.Size(171, 25);
            this.lbCurrentStatus.TabIndex = 3;
            this.lbCurrentStatus.Text = "Status is unknown";
            // 
            // btSetAvailable
            // 
            this.btSetAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btSetAvailable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSetAvailable.ForeColor = System.Drawing.Color.Green;
            this.btSetAvailable.Location = new System.Drawing.Point(5, 90);
            this.btSetAvailable.Name = "btSetAvailable";
            this.btSetAvailable.Size = new System.Drawing.Size(193, 54);
            this.btSetAvailable.TabIndex = 5;
            this.btSetAvailable.Text = "Available";
            this.btSetAvailable.UseVisualStyleBackColor = true;
            this.btSetAvailable.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btSetUnavailable
            // 
            this.btSetUnavailable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btSetUnavailable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSetUnavailable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSetUnavailable.ForeColor = System.Drawing.Color.Red;
            this.btSetUnavailable.Location = new System.Drawing.Point(204, 90);
            this.btSetUnavailable.Name = "btSetUnavailable";
            this.btSetUnavailable.Size = new System.Drawing.Size(193, 54);
            this.btSetUnavailable.TabIndex = 6;
            this.btSetUnavailable.Text = "Not Available";
            this.btSetUnavailable.UseVisualStyleBackColor = true;
            // 
            // lbCurrentStatusTitle
            // 
            this.lbCurrentStatusTitle.AutoSize = true;
            this.lbCurrentStatusTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentStatusTitle.Location = new System.Drawing.Point(12, 16);
            this.lbCurrentStatusTitle.Name = "lbCurrentStatusTitle";
            this.lbCurrentStatusTitle.Size = new System.Drawing.Size(131, 22);
            this.lbCurrentStatusTitle.TabIndex = 7;
            this.lbCurrentStatusTitle.Text = "Current Status:";
            this.lbCurrentStatusTitle.Click += new System.EventHandler(this.label2_Click);
            // 
            // Monitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 150);
            this.Controls.Add(this.lbCurrentStatusTitle);
            this.Controls.Add(this.btSetUnavailable);
            this.Controls.Add(this.btSetAvailable);
            this.Controls.Add(this.lbCurrentStatus);
            this.Controls.Add(this.bt_ReadStatusAloud);
            this.Name = "Monitor";
            this.Text = "Samaritans Eyes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_ReadStatusAloud;
        private System.Windows.Forms.Label lbCurrentStatus;
        private System.Windows.Forms.Button btSetAvailable;
        private System.Windows.Forms.Button btSetUnavailable;
        private System.Windows.Forms.Label lbCurrentStatusTitle;
    }
}

