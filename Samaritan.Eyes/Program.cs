﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Samaritan.Eyes
{
    static class Program
    {
        private static StatusService statusService;
        private static Timer timer;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            statusService = new StatusService();
            statusService.StatusChanged += OnStatusChanged;
            StartCheckingStatus(); 
            
            Application.Run(new Monitor());
        }

        /// <summary>
        /// Starts the timer to check the status on a regular interval
        /// </summary>
        static void StartCheckingStatus()
        {
            timer = new Timer();
            timer.Interval = Convert.ToInt16(ConfigurationManager.AppSettings["CheckStatusPeriodInMilliSeconds"]);
            timer.Tick += CheckStatusOnTick;
            timer.Start();
        }

        static void CheckStatusOnTick(object sender, EventArgs eventArgs)
        {
            timer.Stop();
            statusService.UpdateStatus();
            timer.Start();
        }

        /// <summary>
        /// When the Status changes, we play the message.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void OnStatusChanged(object sender, EventArgs e)
        {
            SoundService.SayText(statusService.CurrentStatus.ToString());
        }
    }
}
