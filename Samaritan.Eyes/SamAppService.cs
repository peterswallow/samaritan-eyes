﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Samaritan.Eyes
{
    public static class SamAppService
    {
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int SendMessage(IntPtr hWnd, int msg, int Param, System.Text.StringBuilder text);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        public static string GetCurrentStatusText()
        {
            List<WinText> windows = new List<WinText>();

            //find the "first" window
            //Window Handle: 0039110E
            //Caption: Dummy
            //Class: WindowsForms10.Window.8.app.0.141b42a_r10_ad1
            //FIND THESE USE SPY++ in Visual Studio->Tools
            var windowClass = "WindowsForms10.Window.8.app.0.141b42a_r10_ad1";
            var windowCaption = "Dummy";
            IntPtr hWnd = FindWindow(windowClass, windowCaption);

            if (hWnd != IntPtr.Zero)
            {
                //find the control window that has the text
                //Image Handle: 003112B8
                //Image Class: WindowsForms10.Window.8.app.0.141b42a_r10_ad1
                //In this case the class of the Image Control in the window, found via Spy++.
                var targetHandle = "00230F06";
                var listBoxClass = "WindowsForms10.COMBOBOX.app.0.141b42a_r10_ad1";
                var targetClass = "WindowsForms10.Window.8.app.0.141b42a_r10_ad1";
                
                IntPtr hEdit = FindWindowEx(hWnd, IntPtr.Zero, listBoxClass, null);

                //initialize the buffer.  using a StringBuilder here
                System.Text.StringBuilder sb = new System.Text.StringBuilder(255);
                    // or length from call with GETTEXTLENGTH

                //get the text from the child control
                int RetVal = SendMessage(hEdit, WM_GETTEXT, sb.Capacity, sb);

                return sb.ToString();

                //windows.Add(new WinText() {hWnd = hWnd, Text = sb.ToString()});

                //find the next window
                //hWnd = FindWindowEx(IntPtr.Zero, hWnd, targetClass, null);
            }

            //do something clever
            //windows.OrderBy(x => x.Text).ToList().ForEach(y => Console.Write("{0} = {1}\n", y.hWnd, y.Text));
            return null;
        }

        private struct WinText
        {
                public IntPtr hWnd;
                public string Text;
        }

        const int WM_GETTEXT = 0x0D;
        const int WM_GETTEXTLENGTH = 0x0E;
    }
}
