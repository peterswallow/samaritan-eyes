﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Speech.Synthesis;


namespace Samaritan.Eyes
{
    public class SoundService
    {
        //https://msdn.microsoft.com/en-us/library/4y171b18(v=vs.110).aspx
        public static void PlaySound()
        {
            SoundPlayer simpleSound = new SoundPlayer(@"c:\Windows\Media\chimes.wav");
            simpleSound.Play(); 
        }

        //https://stackoverflow.com/questions/15387212/how-to-convert-text-string-to-speech-sound
        public static void SayText(string text)
        {
            SpeechSynthesizer synthesizer = new SpeechSynthesizer();
            synthesizer.Volume = 100;  // 0...100
            synthesizer.Rate = -2;     // -10...10

            synthesizer.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult, 3, CultureInfo.CurrentCulture);

            // Synchronous
            //synthesizer.Speak(text);

            // Asynchronous
            synthesizer.SpeakAsync(text);
        }
    }
}
