﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Samaritan.Eyes
{
    public class StatusService
    {
        public enum Status
        {
            Unknown,
            NotSignedIn_NotAvailable,
            SignedIn_NotAvailable,
            SignedIn_Available,
            SignedIn_OnACall,
            SignedIn_Error
        }

        private Status status = Status.Unknown;
        public Status CurrentStatus
        {
            get { return this.status; }
            set
            {
                if (this.status != value)
                {
                    this.status = value;
                    //Status has changed, so fire the event
                    if (this.StatusChanged != null)
                    {
                        this.StatusChanged(this, EventArgs.Empty);
                    }
                }
            }
        }

        public delegate void StatusEventHandler(object sender, EventArgs e);

        public event StatusEventHandler StatusChanged;

        public void UpdateStatus()
        {
            var currentStatusText = SamAppService.GetCurrentStatusText();
            
            if (string.IsNullOrWhiteSpace(currentStatusText))
                return;

            var currentStatus = ConvertTextToStatus(currentStatusText);

            this.CurrentStatus = currentStatus;
        }

        private static Status ConvertTextToStatus(string statusText)
        {
            var enumNumber = Convert.ToInt16(statusText.Substring(0, 1));
            return (Status)enumNumber;
        }
    }
}
